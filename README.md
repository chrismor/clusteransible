# README #

This is a very simple role that will install all the components you need to get MPICH up and running using Ansible to manage the node deployment.

I have used the "package" module, but so far all my testing has been on Ubuntu and Raspbian, both of which work.  It *should* work with RPM based package managers as well.

# ASSUMPTIONS #
Your inventory needs a grouping known as "cluster"
All nodes are considered equal
All nodes are running the same software spec (but may be different architectures)
